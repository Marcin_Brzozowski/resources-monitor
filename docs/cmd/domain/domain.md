# dm-domain

## NAME

domain - Manage domain parameters

## SYNOPSIS

- _dm domain get <domain-name>_
- _dm domain set <domain-name> \[-b|--lower-bound=\<bound\>\] \[-m|--deviation-multiplier=\<multiplier\>\]_
- _dm domain del <domain-name>_
- _dm domain clear_

---

## DESCRIPTION

Use `dm domain` when you want to view or edit parameters of a specific domain

---

## COMMANDS

**get <domain-name>**  
_Show parameters currently used by a given domain_

**set <domain-name> \[-b|--lower-bound\] \[-m|--deviation-multiplier\]**  
_Update parameters of a given domain_

**del <domain-name>**  
_Delete parameters of a given domain_

**clear**  
_Remove parameters of all domains_

---

## OPTIONS

**-b --lower-bound=\<bound\>**  
_Specify lower bound_

**-m --deviation-multiplier=\<multiplier\>**  
_Specify deviation multiplier_

## EXAMPLES

**Viewing parameters of the domain**

> dm domain get artneo.com.pl

```
Properties of artneo.com.pl are as follows:
Lower bound: 2
Deviation multiplier: 3
```

**Setting parameters for the domain**

> dm domain set artneo.com.pl --deviation-multiplier 12 --lower-bound 5

```
Properties of artneo.com.pl are now as follows:
Lower bound: 5
Deviation multiplier: 12
```

**Deleting parameters of the domain**

> dm domain del artneo.com.pl

```
artneo.com.pl properties deleted successfully
Now default properties are in use
```

**Clear all parameters**

> dm domain clear artneo.com.pl

```
Properties cleared successfully
Now every domain use default properties
```

## CONFIGURATION

Everything below this line in this section is selectively included from the [config file](../../config.md)
documentation. The content is the same as what’s found there:

**LOWER_BOUND**  
_Value of lower bound used for all domains by default_  

**DEVIATION_MULTIPLIER**  
_Value of deviation multiplier used for all domains by default_
