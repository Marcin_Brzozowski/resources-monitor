# dm-alert

## NAME

alert - View alerts from the past

## SYNOPSIS

- _dm alert list <date> \[--oneline\] \[--latest\] \[-d|--domains <domains...>\]_
- _dm alert dates \[--limit=<limit>\]_

---

## DESCRIPTION

Use `dm alert` when you want to view alerts from the past

---

## COMMANDS

**list \<date\> \[--oneline\] \[--latest\] \[-d|--domains <domains...>\]**

_List alerts from the specified date_

**dates [--limit=<limit>]**

_Show list of dates available to use_

---

## ARGUMENTS

**date**

Date to view

---

## OPTIONS

**--oneline**

Print alert in compact single line format

**--latest**

Set date argument to the latest available date to show

## EXAMPLES

**Viewing the list of alerts**

> dm alert list --latest

```
Showing 2 of 2 alerts from 2022.12.19
Domain                forum.epgd.com.pl  przemex.com
CPU usage             5.74               15.729
Average CPU           1.417              1.079
CPU deviation         1.404              0.584
Calls                 19081              4119
Average calls         3593.9             737
Calls deviation       2062.634           90.905
Lower bound           2                  2
Deviation multiplier  3                  3
When                  2022.12.19         2022.12.19
```

**Viewing the list of available dates**

> dm alert dates

```
Showing 1 of 1 available dates
1. 2022.12.19
```
