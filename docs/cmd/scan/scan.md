# dm-scan

## NAME

scan - Scan usage of resources of domains on specific date

## SYNOPSIS

- _dm scan \-p|--path=\<path\> \[-d|--date=\<date\>\] \[--latest\] \[--send-email\]_

---

## DESCRIPTION

Use `dm scan` when you want to perform scan on a database and generate alerts

---

## OPTIONS

**-p --path**

Path to the file containing ftp access options

**-d --date**

Specify date to perform a scan

**--latest**

Perform scan on the latest date available in the database

**--send-email**

Send emails containing alert details

## EXAMPLES

**Perform a scan one the latest date**

> dm domain scan -p ftp_options.txt --latest

```
Scan found 2 domains where usage of resources is over allowed
To view alert parameters run, dm alert list <date>
```


## CONFIGURATION

Everything below this line in this section is selectively included from the [config file](../../config.md)
documentation. The content is the same as what’s found there:

**EMAIL_HOST**  
_Email host used to send an email_

**EMAIL_USER**  
_Email login_

**EMAIL_PASSWORD**  
_Email password_

**EMAIL_TO**  
_List of recipients (comma separated)_
