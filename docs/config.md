## CONFIGURATION

**Configuration is placed in .env file. You can configure program activity with the following options:**

**REDIS_URL**   
_URL of the redis database, if you are using localhost you can leave it empty_  
_Valid format: redis[s]://[[username][:password]@][host][:port][/db-number]_

**REDIS_KEY_DURATION**  
_Time in days, which defines how long stored are domain stats in REDIS database  
Default is 20 days_

**SAMPLE_SIZE**  
_Number of domains used to perform analyze, bigger number means better accuracy.  
This option is indirectly connected with REDIS_KEY_DURATION so if you increase this value probably you should also
increase REDIS_KEY_DURATION.  
Default value is 10_

**LOWER_BOUND**  
_Value of lower bound used for all domains by default.  
Default value is 1_

**DEVIATION_MULTIPLIER**  
_Value of deviation multiplier used for all domains by default.  
Default value is 3_

**EMAIL_HOST**  
_Email host used to send an email_

**EMAIL_USER**  
_Email login_

**EMAIL_PASSWORD**  
_Email password_

**EMAIL_TO**  
_List of recipients (comma separated)_

