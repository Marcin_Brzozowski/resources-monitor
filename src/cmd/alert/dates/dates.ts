import {Command, Option} from 'commander';
import {logger} from '../../../logger';
import RedisClient from '../../../redis_client';
import {sortRedisDates, stringToBaseFormat} from '../../../utils';
import {printResult} from '../../../styles';
import {REDIS_URL} from '../../../constants';

interface CommandOptions {
  limit: number;
}

export function makeAlertDatesCommand(alert: Command) {
  const command = alert.command('dates');
  command.addOption(
    new Option('-l, --limit <limit>', 'Amount of dates to list').default(10)
  );
  command.description('Show all available dates');
  command.action(async options => dates(command, options));
}

async function dates(command: Command, options: CommandOptions) {
  try {
    if (isNaN(options.limit)) options.limit = 10;
    const redis = new RedisClient({url: REDIS_URL});

    await redis.connect();
    const dates = await redis.getAlertDates();
    const limitedDates = dates.slice(0, options.limit);
    await redis.close();

    printResult(
      `Showing ${limitedDates.length} of ${dates.length} available dates`
    );
    sortRedisDates(limitedDates).forEach((date, index) => {
      console.log(`${index + 1}. ${stringToBaseFormat(date)}`);
    });
  } catch (err) {
    logger.error(err);
  }
}
