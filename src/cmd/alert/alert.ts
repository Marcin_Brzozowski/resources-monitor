import {Command} from 'commander';
import {makeAlertDatesCommand} from './dates/dates';
import {makeAlertListCommand} from './list/list';

export function makeAlertCommand() {
  const alert = new Command('alert');
  alert.description('Manage alerts');

  makeAlertListCommand(alert);
  makeAlertDatesCommand(alert);

  return alert;
}
