import {Command} from 'commander';
import {logger} from '../../../logger';
import {
  printAlerts,
  sortRedisDates,
  stringToBaseFormat,
  validateDate,
} from '../../../utils';
import RedisClient from '../../../redis_client';
import {printError, printInfo, printResult, sprintTip} from '../../../styles';
import {REDIS_URL} from '../../../constants';

interface CommandOptions {
  date?: string;
  domain?: string[];
  oneline?: boolean;
  latest?: boolean;
}

export function makeAlertListCommand(alert: Command) {
  const command = alert.command('list');
  command.argument('[date]', 'Date to list');
  command.option('-d, --domain <domains...>', 'Domains to show');
  command.option('--oneline', 'View alert in short one line format');
  command.option('--latest', 'Show alerts from the latest analysis');
  command.description('List all alerts of the day');
  command.action(
    async (date, options) =>
      await list(command, {
        date,
        ...options,
      })
  );
}

async function list(command: Command, options: CommandOptions) {
  try {
    if (!(await validateOptions(command, options))) {
      return;
    }
    const redis = new RedisClient({url: REDIS_URL});
    const {date, domain, oneline, latest} = options;

    await redis.connect();
    const dates = sortRedisDates(await redis.getListOfDates());

    let finalDate = date ? date : '';
    if (latest) {
      finalDate = dates[0];
    }

    const alerts = await redis.getAlertsByDate(finalDate);

    await redis.close();

    if (alerts.length === 0) {
      printResult('No alerts');
      console.log(
        `Try running ${sprintTip(
          'dm alert dates'
        )} to list all available dates when alerts occurred`
      );
      return;
    }

    const filteredAlerts = alerts.filter(alert => {
      if (domain === undefined) return true;

      for (const d of domain) {
        if (alert.name.includes(d)) {
          return true;
        }
      }
      return false;
    });

    printResult(
      `Showing ${filteredAlerts.length} of ${
        alerts.length
      } alerts from ${stringToBaseFormat(finalDate)}`
    );
    printAlerts(filteredAlerts, oneline);
  } catch (err) {
    logger.error(err);
  }
}

async function validateOptions(command: Command, options: CommandOptions) {
  const {date, latest} = options;

  if (latest && date) {
    printError('You must choose only one date argument or --latest flag');
    return false;
  }

  if (latest) {
    return true;
  }

  if (!date) {
    printError('You must specify date to list or use --latest flag');
    return false;
  }

  if (date && !validateDate(date)) {
    printError('Invalid date format');
    printInfo('Correct format is: YYYY.MM.DD');
    return false;
  }
  return true;
}
