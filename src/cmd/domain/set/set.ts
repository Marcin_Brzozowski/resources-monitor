import {Command} from 'commander';
import RedisClient from '../../../redis_client';
import {logger} from '../../../logger';
import {printError, printInfo, printResult} from '../../../styles';
import {DEFAULT_DOMAIN_PARAMETERS, REDIS_URL} from '../../../constants';

interface CommandOptions {
  domain: string;
  lowerBound?: string;
  deviationMultiplier?: string;
}

export function makeSetDomainCommand(domain: Command) {
  const command = domain.command('set');
  command.argument('<domain>', 'domain name');
  command.option('-b, --lower-bound <number>', 'lower bound');
  command.option('-m, --deviation-multiplier <number>', 'deviation multiplier');
  command.description('Set domain parameters');
  command.action(async (domain, options) => set(command, {domain, ...options}));
}

async function set(command: Command, options: CommandOptions) {
  try {
    validateOptions(command, options);
    const redis = new RedisClient({url: REDIS_URL});
    const {lowerBound, deviationMultiplier, domain} = options;

    const params = DEFAULT_DOMAIN_PARAMETERS;

    if (lowerBound) params.lowerBound = parseInt(lowerBound);
    if (deviationMultiplier)
      params.deviationMultiplier = parseInt(deviationMultiplier);

    await redis.connect();
    await redis.setDomainParameters(domain, params);
    const parameters = await redis.getDomainParameters(domain);
    await redis.close();

    printResult(`Properties of ${domain} are now as follows:`);
    printInfo(`Lower bound: ${parameters.lowerBound}`);
    printInfo(`Deviation multiplier: ${parameters.deviationMultiplier}`);
  } catch (err) {
    logger.error(err);
  }
}

function validateOptions(command: Command, options: CommandOptions) {
  const {lowerBound, deviationMultiplier} = options;

  if (!lowerBound && !deviationMultiplier) {
    printError('No parameters were specified');
  }

  if (lowerBound && isNaN(Number(lowerBound)))
    printError('Parameters must be numbers');
  if (deviationMultiplier && isNaN(Number(deviationMultiplier)))
    printError('Parameters must be numbers');
}
