import {Command} from 'commander';
import RedisClient from '../../../redis_client';
import {logger} from '../../../logger';
import {printInfo, printResult} from '../../../styles';
import {REDIS_URL} from '../../../constants';

interface CommandOptions {
  domain: string;
}

export function makeGetDomainCommand(domain: Command) {
  const command = domain.command('get');
  command.argument('<domain>', 'domain name');
  command.description('Show domain parameters');
  command.action(async domain => get(command, {domain}));
}

async function get(command: Command, options: CommandOptions) {
  try {
    const {domain} = options;
    const redis = new RedisClient({url: REDIS_URL});
    await redis.connect();
    const parameters = await redis.getDomainParameters(domain);
    await redis.close();

    printResult(`Properties of ${domain} are as follows:`);
    printInfo(`Lower bound: ${parameters.lowerBound}`);
    printInfo(`Deviation multiplier: ${parameters.deviationMultiplier}`);
  } catch (err) {
    logger.error(err);
  }
}
