import {Command} from 'commander';
import RedisClient from '../../../redis_client';
import {logger} from '../../../logger';
import {printInfo, printResult} from '../../../styles';
import {REDIS_URL} from '../../../constants';

interface CommandOptions {
  domain: string;
}

export function makeDelDomainCommand(domain: Command) {
  const command = domain.command('del');
  command.argument('<domain>', 'domain name');
  command.description('Delete parameters of a specified domain');
  command.action(async domain => del(command, {domain}));
}

async function del(command: Command, options: CommandOptions) {
  try {
    const {domain} = options;
    const redis = new RedisClient({url: REDIS_URL});

    await redis.connect();
    await redis.delDomainParameters(domain);
    await redis.close();

    printResult(`${domain} properties deleted successfully`);
    printInfo('Now default properties are in use');
  } catch (err) {
    logger.error(err);
  }
}
