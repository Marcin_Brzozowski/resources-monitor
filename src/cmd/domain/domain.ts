import {Command} from 'commander';
import {makeGetDomainCommand} from './get/get';
import {makeSetDomainCommand} from './set/set';
import {makeDelDomainCommand} from './del/del';
import {makeClearDomainCommand} from './clear/clear';

export function makeDomainCommand() {
  const command = new Command('domain');
  command.description('Manage domain parameters');

  makeGetDomainCommand(command);
  makeSetDomainCommand(command);
  makeDelDomainCommand(command);
  makeClearDomainCommand(command);

  return command;
}
