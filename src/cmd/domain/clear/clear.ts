import {Command} from 'commander';
import RedisClient from '../../../redis_client';
import {logger} from '../../../logger';
import {printInfo, printResult} from '../../../styles';
import {REDIS_URL} from '../../../constants';

export function makeClearDomainCommand(domain: Command) {
  const command = domain.command('clear');
  command.description('Delete parameters of all domains');
  command.action(async () => await clear());
}

async function clear() {
  try {
    const redis = new RedisClient({url: REDIS_URL});

    await redis.connect();
    await redis.clearDomainsParameters();
    await redis.close();

    printResult('Properties cleared successfully');
    printInfo('Now every domain use default properties');
  } catch (err) {
    logger.error(err);
  }
}
