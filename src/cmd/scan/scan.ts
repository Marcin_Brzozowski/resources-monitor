import {Command} from 'commander';
import {logger} from '../../logger';
import {
  checkIfFileExists,
  stringToBaseFormat,
  stringToRedisFormat,
  validateDate,
} from '../../utils';
import chalk from 'chalk';
import RedisClient from '../../redis_client';
import Analyser from '../../analyser';
import FtpClient from '../../ftp_client';
import ReportParser from '../../report_parser';
import archiver from 'archiver';
import {createWriteStream} from 'fs';
import fs from 'node:fs/promises';
import dotenv from 'dotenv';
import cliProgress from 'cli-progress';
import nodemailer from 'nodemailer';
import {ArrayOfDomainAlerts, FtpAccessOptions} from '../../typeguards';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import {printError, printInfo, printResult, sprintTip} from '../../styles';
import {
  EMAIL_HOST,
  EMAIL_PASSWORD,
  EMAIL_TO,
  EMAIL_USER,
  REDIS_URL,
} from '../../constants';

interface CommandOptions {
  path: string;
  date?: string;
  sendEmail?: boolean;
  latest?: boolean;
}

export function makeScanCommand() {
  const command = new Command('scan');
  command.description('Scan resources usage of domains on specific date');
  command.requiredOption(
    '-p, --path <path>',
    'Path to the file containing FTP access options'
  );
  command.option('-d, --date <date>', 'Date to scan');
  command.option('--send-email', 'Send email with alert');
  command.option('--latest', 'Scan latest available date');
  command.action(async options => scan(command, options));
  return command;
}

async function scan(command: Command, options: CommandOptions) {
  try {
    if (!(await validateOptions(command, options))) return;
    const {date, latest, path} = options;

    await fetch(path);
    const alerts = await scanByDay(latest ? '' : date);

    printResult(
      `Scan found ${chalk.redBright(
        alerts.length
      )} domains where usage of resources is over allowed`
    );
    console.log(
      `To view alert parameters run, ${sprintTip('dm alert list <date>')}`
    );

    if (alerts.length === 0) {
      return;
    }

    if (options.sendEmail) {
      await sendEmails(alerts);
    }
  } catch (err) {
    logger.error(err);
  }
}

async function sendEmails(alerts: ArrayOfDomainAlerts) {
  const transporter = nodemailer.createTransport({
    host: EMAIL_HOST,
    port: 587,
    secure: false,
    auth: {
      user: EMAIL_USER,
      pass: EMAIL_PASSWORD,
    },
  });

  const emails: Promise<SMTPTransport.SentMessageInfo>[] = [];
  for (const alert of alerts) {
    const email = {
      from: `"Domain Monitor" <${EMAIL_USER}>`,
      to: EMAIL_TO,
      subject: `Domain Monitor - ${alert.date}, ${alert.name}`,
      text: 'Alert message',
      html: '<h1>Alert message</h1>',
    };
    emails.push(transporter.sendMail(email));
  }

  Promise.all(emails)
    .then(() => {
      printInfo('All emails have been sent successfully');
    })
    .catch(err => {
      printError('Sending emails failed');
      throw err;
    });
}

async function fetch(path: string) {
  const ftpOptions = await getFtpOptions(path);
  const ftp = new FtpClient(ftpOptions);
  const redis = new RedisClient({url: REDIS_URL});

  await ftp.connect();

  const files = (await ftp.getListOfFiles()).filter(file =>
    file.name.endsWith('.txt')
  );
  if (files.length === 0) {
    await ftp.close();
    return;
  }

  const parser = new ReportParser();

  const dates = files.reduce<Set<string>>((acc, file) => {
    parser.loadReport(file, '');
    const {date} = parser.getDomainDetails();
    acc.add(date);
    return acc;
  }, new Set());

  const multiBar = new cliProgress.MultiBar(
    {clearOnComplete: true},
    cliProgress.Presets.shades_classic
  );
  await redis.connect();

  for (const date of dates) {
    const archive = archiver('tar', {
      gzip: true,
      zlib: {level: 9},
    });
    const filteredFiles = files.filter(file => file.name.includes(date));
    const archivePath = `./${date.replace(/\./g, '_')}.tar.gz`;
    const output = createWriteStream(archivePath);
    archive.pipe(output);

    const progress = multiBar.create(filteredFiles.length, 0, 0, {
      format: `Processing ${date} [{bar}] {percentage}% | ETA: {eta}s | {value}/{total}`,
    });
    for (const file of filteredFiles) {
      const stream = await ftp.getFileContentByFileInfo(file);
      archive.append(stream, {name: file.name});
      await parser.loadReportFromStream(file, stream);
      const details = parser.getDomainDetails();
      await redis.addDetails(details);
      progress.increment();
    }

    progress.stop();
    await archive.finalize();
    await ftp.upload(archivePath);

    await fs.unlink(archivePath);
    for (const file of filteredFiles) {
      await ftp.remove(file.name);
    }
  }

  multiBar.stop();
  await redis.close();
  await ftp.close();
}

async function scanByDay(date = '') {
  const redis = new RedisClient({url: REDIS_URL});
  await redis.connect();
  const availableDates = new Set(
    (await redis.getListOfDates()).map(date => stringToBaseFormat(date))
  );
  if (date !== '' && !availableDates.has(date)) {
    printError("Selected date isn't available to scan");
    printInfo('Probably you selected date from future or from too long ago');
    process.exit();
  }
  const analyser = new Analyser(redis, stringToRedisFormat(date));
  const results = await analyser.getAlarmingDomains();
  await redis.close();
  return results;
}

export async function getFtpOptions(path: string): Promise<FtpAccessOptions> {
  const buffer = await fs.readFile(path);

  const object = FtpAccessOptions.safeParse(
    buffer
      .toString()
      .split(/\r?\n/)
      .reduce<object>((acc: object, val: string) => {
        const config = Buffer.from(val);
        acc = {
          ...acc,
          ...dotenv.parse(config),
        };
        return acc;
      }, {})
  );

  if (!object.success) {
    throw new Error(
      'Specified FTP access file miss some attributes or have wrong syntax'
    );
  }

  return object.data;
}

async function validateOptions(command: Command, options: CommandOptions) {
  const {date, latest, path} = options;

  if (!(await checkIfFileExists(path))) {
    printError(
      `There is no such file as "${path}", please check the path you've specified`
    );
    return false;
  }

  if (!latest && !date) {
    printError('You must specify date to scan or use --latest flag');
    return false;
  }

  if (date && !validateDate(date)) {
    printError('Invalid date format');
    printInfo('Correct format is: YYYY.MM.DD');
    return false;
  }
  return true;
}
