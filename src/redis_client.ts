import {
  createClient,
  RedisClientOptions,
  RedisClientType,
  RedisModules,
} from 'redis';
import {daysToSeconds, stringToRedisFormat} from './utils';
import {
  ArrayOfDomainAlerts,
  ArrayOfDomainParameters,
  DomainAlert,
  DomainDetails,
  DomainParameters,
  RecordOfDomainParameters,
  RecordOfDomainStats,
} from './typeguards';
import {printError} from './styles';
import {DEFAULT_DOMAIN_PARAMETERS, REDIS_KEY_DURATION} from './constants';
import {z} from 'zod';

type ClientOptions = RedisClientOptions<
  RedisModules,
  Record<string, never>,
  Record<string, never>
>;

export default class RedisClient {
  private readonly client: RedisClientType;
  private readonly clientOptions: ClientOptions;
  private expireTime = daysToSeconds(REDIS_KEY_DURATION);

  constructor(clientOptions: ClientOptions) {
    this.clientOptions = clientOptions;
    this.client = createClient(clientOptions);
  }

  async connect() {
    try {
      await this.client.connect();
    } catch (err) {
      printError(
        `Connecting with ${
          this.clientOptions.url || 'localhost redis db'
        } failed`
      );
      throw err;
    }
  }

  async close() {
    try {
      await this.client.quit();
    } catch (err) {
      printError(`Closing a connection with ${this.clientOptions.url} failed`);
      throw err;
    }
  }

  async getStatsByDate(date: string): Promise<RecordOfDomainStats> {
    const parsedDate = stringToRedisFormat(date);
    const value = RecordOfDomainStats.safeParse(
      await this.client.json.get(parsedDate)
    );
    if (!value.success) {
      return {};
    }

    return value.data;
  }

  async addDetails(domain: DomainDetails) {
    const date = stringToRedisFormat(domain.date);
    const name = stringToRedisFormat(domain.name);

    try {
      const {calls, cpuTimeUsed} = domain;
      await this.client.json.set(date, '$', {}, {NX: true});
      await this.client.expire(date, this.expireTime, 'NX');
      await this.client.json.set(
        date,
        this.getPath(name),
        {calls, cpuTimeUsed},
        {NX: true}
      );
    } catch (err) {
      printError(`Adding details of ${domain.name} on ${domain.date} failed`);
      throw err;
    }
  }

  async addAlert(alert: DomainAlert) {
    const parsedDate = stringToRedisFormat(alert.date);

    try {
      await this.client.json.set('ALERTS', '$', {}, {NX: true});
      await this.client.json.set('ALERTS', this.getPath(parsedDate), [], {
        NX: true,
      });
      const result = z
        .array(ArrayOfDomainAlerts)
        .nonempty()
        .safeParse(
          await this.client.json.get('ALERTS', {
            path: this.getPath(parsedDate),
          })
        );

      if (!result.success) {
        return;
      }

      const domains = result.data[0];

      if (domains.find(domain => domain.name === alert.name)) return;

      await this.client.json.arrAppend('ALERTS', this.getPath(parsedDate), {
        ...alert,
      });
    } catch (err) {
      printError(`Adding new alert of ${alert.name} on ${alert.date} failed`);
      throw err;
    }
  }

  async getAlertsByDate(date: string): Promise<ArrayOfDomainAlerts> {
    try {
      const value = z
        .array(ArrayOfDomainAlerts)
        .nonempty()
        .safeParse(
          await this.client.json.get('ALERTS', {
            path: `$.${stringToRedisFormat(date)}`,
          })
        );
      if (!value.success) {
        return [];
      }

      return value.data[0];
    } catch (err) {
      printError(`Getting list of alerts from ${date} failed`);
      throw err;
    }
  }

  async getAlertDates(): Promise<string[]> {
    try {
      const dates = z
        .array(z.string())
        .nonempty()
        .safeParse(await this.client.json.objKeys('ALERTS'));
      if (!dates.success) {
        return [];
      }

      return dates.data;
    } catch (err) {
      printError('Getting a list of alert dates failed');
      throw err;
    }
  }

  async getListOfDates(): Promise<string[]> {
    try {
      const keys = z.array(z.string()).safeParse(await this.client.keys('*'));
      if (!keys.success) {
        return [];
      }

      return keys.data.filter(key => key.match(/(\d{4}_\d{2}_\d{2})/g));
    } catch (err) {
      printError('Getting a list of dates failed');
      throw err;
    }
  }

  async getAllDomainsParameters(): Promise<RecordOfDomainParameters> {
    try {
      const value = RecordOfDomainParameters.safeParse(
        await this.client.json.get('domains')
      );
      if (!value.success) {
        return {};
      }
      return value.data;
    } catch (err) {
      printError('Fetching parameters of all domains failed');
      throw err;
    }
  }

  async getDomainParameters(name: string): Promise<DomainParameters> {
    const parsedName = stringToRedisFormat(name);
    try {
      const value = ArrayOfDomainParameters.safeParse(
        await this.client.json.get('domains', {
          path: this.getPath(parsedName),
        })
      );
      if (!value.success) {
        return DEFAULT_DOMAIN_PARAMETERS;
      }
      return value.data[0];
    } catch (err) {
      printError(`Getting parameters of ${name} failed`);
      throw err;
    }
  }

  async setDomainParameters(domain: string, params: DomainParameters) {
    const parsedName = stringToRedisFormat(domain);
    try {
      await this.client.json.set('domains', '$', {
        [parsedName]: {
          ...params,
        },
      });
    } catch (err) {
      printError(`Setting parameters of ${domain} failed`);
      throw err;
    }
  }

  async clearDomainsParameters() {
    try {
      await this.client.json.del('domains');
    } catch (err) {
      printError('Clearing domain parameters failed');
      throw err;
    }
  }

  async delDomainParameters(name: string) {
    const parsedName = stringToRedisFormat(name);
    try {
      await this.client.json.del('domains', this.getPath(parsedName));
    } catch (err) {
      printError(`Deleting parameters of ${name} failed`);
      throw err;
    }
  }

  private getPath(...strings: string[]) {
    return `$.${strings.join('.')}`;
  }
}
