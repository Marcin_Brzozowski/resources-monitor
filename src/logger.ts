import {pino} from 'pino';
import {PINO_LOG_LEVEL} from './constants';

export const logger = pino({
  level: PINO_LOG_LEVEL,
  transport: {
    target: 'pino-pretty',
    options: {colorize: true},
  },
});
