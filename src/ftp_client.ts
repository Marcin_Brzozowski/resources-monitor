import * as ftp from 'basic-ftp';
import {AccessOptions, FileInfo} from 'basic-ftp';
import {Readable, Writable} from 'stream';
import {streamToString} from './utils';
import {printError, printWarn} from './styles';

export default class FtpClient {
  private readonly client: ftp.Client;
  private readonly accessOptions: AccessOptions;

  constructor(accessOptions: AccessOptions) {
    this.client = new ftp.Client();
    this.accessOptions = accessOptions;
  }

  public async connect() {
    try {
      if (!this.client.closed) {
        printWarn(
          `Connection with ${this.accessOptions.host} (FTP) is already open`
        );
        return;
      }
      await this.client.access(this.accessOptions);
    } catch (err) {
      this.client.close();
      printError(`Connecting with ${this.accessOptions.host} (FTP) failed`);
      throw err;
    }
  }

  public close() {
    try {
      if (this.client.closed) {
        printWarn(
          `Connection with ${this.accessOptions.host} (FTP) is already open`
        );
        return;
      }
      this.client.close();
    } catch (err) {
      printError(
        `Closing connection with ${this.accessOptions.host} (FTP) failed`
      );
      throw err;
    }
  }

  public async getListOfFiles(): Promise<FileInfo[]> {
    try {
      if (this.client.closed) {
        printWarn(`Connection with ${this.accessOptions.host} (FTP) is closed`);
        return [];
      }

      return this.client.list();
    } catch (err) {
      printWarn('Getting a list of files failed');
      return [];
    }
  }

  async getFileContentByName(filename: string): Promise<string> {
    try {
      if (this.client.closed) {
        printWarn(
          `Connection with ${this.accessOptions.host} (FTP), is already open`
        );
        return '';
      }

      const stream = new EchoStream();
      await this.client.downloadTo(stream, filename);
      const s = new Readable();
      s._read = () => {};
      s.push(stream.data, 'utf8');
      s.push(null);
      return streamToString(s);
    } catch (err) {
      printError(`Getting content of ${filename} failed`);
      throw err;
    }
  }

  async getFileContentByFileInfo(file: FileInfo) {
    return this.getFileContentByName(file.name);
  }

  async upload(path: string) {
    try {
      await this.client.uploadFrom(path, path);
    } catch (err) {
      printError(`Uploading a ${path} failed`);
      throw err;
    }
  }

  async remove(path: string) {
    try {
      await this.client.remove(path);
    } catch (err) {
      printError(`Deleting a ${path} failed`);
      throw err;
    }
  }
}

/* eslint-disable @typescript-eslint/no-explicit-any */
class EchoStream extends Writable {
  data = '';

  _write(
    chunk: any,
    enc: BufferEncoding,
    next: (error?: Error | null) => void
  ) {
    this.data += chunk.toString();
    next();
  }
}

/* eslint-enable @typescript-eslint/no-explicit-any */
