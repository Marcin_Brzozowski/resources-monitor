import {envToNumber} from './utils';
import {DomainParameters} from './typeguards';
import * as dotenv from 'dotenv';

dotenv.config();

export const PINO_LOG_LEVEL = process.env.PINO_LOG_LEVEL || 'info';

export const REDIS_URL = process.env.REDIS_URL || '';
export const REDIS_KEY_DURATION = envToNumber(
  process.env.REDIS_KEY_DURATION,
  20
);

export const ANALYZE_SAMPLE_SIZE = envToNumber(process.env.SAMPLE_SIZE, 10);

export const EMAIL_HOST = process.env.EMAIL_HOST || '';
export const EMAIL_USER = process.env.EMAIL_USER || '';
export const EMAIL_PASSWORD = process.env.EMAIL_PASSWORD || '';
export const EMAIL_TO = process.env.EMAIL_TO || '';

export const GLOBAL_LOWER_BOUND = envToNumber(process.env.LOWER_BOUND, 2);
export const GLOBAL_DEVIATION_MULTIPLIER = envToNumber(
  process.env.DEVIATION_MULTIPLIER,
  3
);
export const DEFAULT_DOMAIN_PARAMETERS: DomainParameters = {
  lowerBound: GLOBAL_LOWER_BOUND,
  deviationMultiplier: GLOBAL_DEVIATION_MULTIPLIER,
};
