import {FileInfo} from 'basic-ftp';
import {DomainDetails} from './typeguards';

const BASE_DOMAIN_DETAILS: DomainDetails = {
  name: '',
  date: '',
  calls: 0,
  cpuTimeUsed: 0,
};

export default class ReportParser {
  private report: FileInfo = <FileInfo>{};
  private contentLines: string[] = [];

  private isLoaded = false;
  private isParsed = false;

  private domainDetails = BASE_DOMAIN_DETAILS;

  public loadReport(report: FileInfo, content: string) {
    this.resetParser();

    this.report = report;
    this.contentLines = content.split(/\r?\n/, 5);
    this.isLoaded = true;
  }

  public async loadReportFromStream(report: FileInfo, content: string) {
    this.loadReport(report, content);
  }

  public getDomainDetails(): DomainDetails {
    if (!this.isParsed) this.parseReport();
    return this.domainDetails;
  }

  private resetParser() {
    this.isParsed = false;
    this.domainDetails = BASE_DOMAIN_DETAILS;
  }

  private isReportEmpty() {
    return (
      !this.contentLines[0] ||
      this.report.size <= 68 ||
      this.contentLines[0].trim().includes('Bledny parametr daty') ||
      this.contentLines[2].trim().includes('Brak wywolan domeny')
    );
  }

  private parseReport() {
    if (!this.isLoaded) throw new Error('Parsing failed, no file is loaded');

    const {name} = this.report;
    this.domainDetails.name = name.slice(11, -11);
    this.domainDetails.date = name.slice(0, 10);

    if (this.isReportEmpty()) {
      this.domainDetails.calls = 0;
      this.domainDetails.cpuTimeUsed = 0;
      return;
    }

    this.domainDetails.calls = Number.parseInt(
      this.contentLines[2].slice(9).trim()
    );
    this.domainDetails.cpuTimeUsed = Number.parseInt(
      this.contentLines[3].slice(16)
    );
  }
}
