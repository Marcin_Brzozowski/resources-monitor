import {z} from 'zod';

export const DomainDetails = z.object({
  name: z.string(),
  date: z.string(),
  calls: z.number(),
  cpuTimeUsed: z.number(),
});

export type DomainDetails = z.infer<typeof DomainDetails>;

export const FtpAccessOptions = z.object({
  host: z.string(),
  user: z.string(),
  password: z.string(),
});

export type FtpAccessOptions = z.infer<typeof FtpAccessOptions>;

export const DomainsStats = z.object({
  calls: z.number(),
  cpuTimeUsed: z.number(),
});

export type DomainStats = z.infer<typeof DomainsStats>;

export const DomainParameters = z.object({
  lowerBound: z.number(),
  deviationMultiplier: z.number(),
});

export type DomainParameters = z.infer<typeof DomainParameters>;

export const AnalyzedDomain = z.object({
  calls: z.number(),
  cpu: z.number(),
  avgCalls: z.number(),
  avgCPU: z.number(),
  cpuDeviation: z.number(),
  callsDeviation: z.number(),
});

export type AnalyzedDomain = z.infer<typeof AnalyzedDomain>;

export const DomainAlert = z
  .object({
    name: z.string(),
    date: z.string(),
  })
  .merge(AnalyzedDomain)
  .merge(DomainParameters);

export type DomainAlert = z.infer<typeof DomainAlert>;

export const RecordOfDomainStats = z.record(z.string(), DomainsStats);
export type RecordOfDomainStats = z.infer<typeof RecordOfDomainStats>;

export const RecordOfDomainParameters = z.record(z.string(), DomainParameters);
export type RecordOfDomainParameters = z.infer<typeof RecordOfDomainParameters>;

export const ArrayOfDomainAlerts = z.array(DomainAlert);
export type ArrayOfDomainAlerts = z.infer<typeof ArrayOfDomainAlerts>;

export const ArrayOfDomainParameters = z.array(DomainParameters).nonempty();
export type ArrayOfDomainParameters = z.infer<typeof ArrayOfDomainAlerts>;
