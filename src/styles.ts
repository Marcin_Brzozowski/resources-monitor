import chalk from 'chalk';

const result = chalk.bold.greenBright;
const error = chalk.red.bold;
const info = chalk.italic;
const tip = chalk.italic.underline;
const warn = chalk.hex('#028e8e');

export function printError(message: string) {
  console.log(error(message));
}

export function printWarn(message: string) {
  console.log(warn(message));
}

export function printInfo(message: string) {
  console.log(info(message));
}

export function printResult(message: string) {
  console.log(result(message));
}

export function printTip(message: string) {
  console.log(tip(message));
}

export function sprintTip(message: string) {
  return tip(message);
}
