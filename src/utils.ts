import {Readable} from 'stream';
import chalk from 'chalk';
import fs from 'node:fs';
import {ArrayOfDomainAlerts} from './typeguards';
import Table from 'easy-table';

export function stringToRedisFormat(date: string) {
  if (!date) return '';
  return date.replace(/\./g, '_');
}

export function stringToBaseFormat(date: string) {
  return date.replace(/_/g, '.');
}

export function daysToSeconds(days: number) {
  return 3600 * 24 * days;
}

export function envToNumber(
  envVariable: string | undefined,
  defaultValue: number
) {
  return envVariable ? parseFloat(envVariable) : defaultValue;
}

export function checkIfFileExists(path: string) {
  return fs.promises
    .access(path, fs.promises.constants.F_OK)
    .then(() => true)
    .catch(() => false);
}

export function streamToString(stream: Readable): Promise<string> {
  const chunks: Buffer[] = [];
  return new Promise((resolve, reject) => {
    stream.on('data', chunk => chunks.push(Buffer.from(chunk)));
    stream.on('error', err => reject(err));
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
  });
}

export function validateDate(date: string) {
  return date.match(/^(\d{4}\.\d{2}\.\d{2})$/g);
}

export function sortRedisDates(dates: string[]) {
  dates.sort((a, b) => {
    const aShattered = a.split('_').map(x => parseInt(x));
    const bShattered = b.split('_').map(x => parseInt(x));
    return (
      new Date(bShattered[0], bShattered[1], bShattered[2]).getTime() -
      new Date(aShattered[0], aShattered[1], aShattered[2]).getTime()
    );
  });
  return dates;
}

export function printAlerts(alerts: ArrayOfDomainAlerts, oneline?: boolean) {
  if (oneline) {
    alerts.forEach(alert => {
      const {name, date, cpu, avgCPU, cpuDeviation} = alert;
      console.log(
        `${chalk.hex('#028e8e').bold(name)} on ${chalk
          .hex('#d4a100')
          .bold(date)} used ${cpu}% of cpu which is ${chalk.bold.red(
          Math.round((cpu - avgCPU) / cpuDeviation)
        )} times larger than the standard deviation`
      );
    });
    return;
  }

  const t = new Table();

  alerts.forEach(alert => {
    t.cell(chalk.bold.yellow('Domain'), alert.name);
    t.cell(chalk.bold.yellow('CPU usage'), alert.cpu);
    t.cell(chalk.bold.yellow('Average CPU'), alert.avgCPU);
    t.cell(chalk.bold.yellow('CPU deviation'), alert.cpuDeviation);
    t.cell(chalk.bold.yellow('Calls'), alert.calls);
    t.cell(chalk.bold.yellow('Average calls'), alert.avgCalls);
    t.cell(chalk.bold.yellow('Calls deviation'), alert.callsDeviation);
    t.cell(chalk.bold.yellow('Lower bound'), alert.lowerBound);
    t.cell(
      chalk.bold.yellow('Deviation multiplier'),
      alert.deviationMultiplier
    );
    t.cell(chalk.bold.yellow('When'), alert.date);
    t.newRow();
  });
  console.log(t.sort(['CPU usage|des']).printTransposed());
}
