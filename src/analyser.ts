import RedisClient from './redis_client';
import {sortRedisDates, stringToBaseFormat, stringToRedisFormat} from './utils';
import {
  ANALYZE_SAMPLE_SIZE,
  GLOBAL_DEVIATION_MULTIPLIER,
  GLOBAL_LOWER_BOUND,
} from './constants';
import {AnalyzedDomain, ArrayOfDomainAlerts, DomainStats} from './typeguards';

export default class Analyser {
  private readonly redis: RedisClient;
  private readonly sampleSize: number = ANALYZE_SAMPLE_SIZE;
  private readonly date;

  private samples: Record<string, DomainStats>[] = [];
  private totalCpuTimePerDay: Record<string, number> = {};
  private dates: string[] = [];
  private results: Record<string, AnalyzedDomain> = {};

  constructor(redis: RedisClient, date = '') {
    this.redis = redis;
    this.date = date;
  }

  async setup() {
    const dates = await this.redis.getListOfDates();
    this.dates = sortRedisDates(dates);

    if (this.date !== '') {
      const initialDateIndex = this.dates.indexOf(
        stringToRedisFormat(this.date)
      );
      if (initialDateIndex === -1) this.dates = [];
      this.dates = this.dates.slice(initialDateIndex);
    }

    for (const date of this.dates.slice(0, this.sampleSize + 1)) {
      const domainsOfDay = await this.redis.getStatsByDate(date);
      this.samples.push(domainsOfDay);

      for (const domain of Object.keys(domainsOfDay)) {
        if (!this.totalCpuTimePerDay[date]) this.totalCpuTimePerDay[date] = 0;
        this.totalCpuTimePerDay[date] += domainsOfDay[domain].cpuTimeUsed;
      }
    }
  }

  parse() {
    if (this.dates.length === 0) return;

    const results: Record<string, AnalyzedDomain> = {};
    const domains = Object.keys(this.samples[0]);
    for (const domain of domains) {
      const callsCollection: number[] = [];
      const cpuCollection: number[] = [];
      for (const [index, domains] of this.samples.slice(1).entries()) {
        let cpuRate = 0;
        let calls = 0;
        if (Object.prototype.hasOwnProperty.call(domains, domain)) {
          cpuRate = domains[domain].cpuTimeUsed;
          calls = domains[domain].calls;
        }

        callsCollection.push(calls);
        cpuCollection.push(cpuRate);
      }

      results[domain] = {
        calls: round1000(this.samples[0][domain].calls),
        avgCalls: round1000(
          callsCollection.reduce((acc, val) => acc + val, 0) / this.sampleSize
        ),
        callsDeviation: round1000(getStandardDeviation(callsCollection)),

        cpu: round1000(
          (this.samples[0][domain].cpuTimeUsed /
            86400000)
        )*100,
        avgCPU: round1000(
          cpuCollection.reduce((acc, val) => acc + val, 0) / (this.sampleSize * 86400000)
        )*100,
        cpuDeviation: round1000(getStandardDeviation(cpuCollection) / 86400000)*100,
      };
    }
    this.results = results;
  }

  async getAlarmingDomains() {
    await this.setup();
    this.parse();
    if (this.dates.length === 0) return [];
    if (this.samples.length < this.sampleSize / 2) return [];

    const domainsToAlarm: ArrayOfDomainAlerts = [];
    const domainsParameters = await this.redis.getAllDomainsParameters();
    const domains = Object.keys(this.samples[0]);

    let lowerBound: number = GLOBAL_LOWER_BOUND;
    let deviationMultiplier: number = GLOBAL_DEVIATION_MULTIPLIER;

    for (const domain of domains) {
      const {cpu, avgCPU, cpuDeviation} = this.results[domain];
      if (
        domainsParameters &&
        domainsParameters[domain] &&
        domainsParameters[domain].lowerBound !== undefined
      ) {
        lowerBound = domainsParameters[domain].lowerBound!;
      }
      if (
        domainsParameters &&
        domainsParameters[domain] &&
        domainsParameters[domain].deviationMultiplier !== undefined
      ) {
        deviationMultiplier = domainsParameters[domain].deviationMultiplier!;
      }
      if (cpu < lowerBound) continue;
      if (cpu - avgCPU > deviationMultiplier * cpuDeviation) {
        const alert = {
          name: stringToBaseFormat(domain),
          date: stringToBaseFormat(this.dates[0]),
          ...this.results[domain],
          lowerBound,
          deviationMultiplier,
        };
        domainsToAlarm.push(alert);
        await this.redis.addAlert(alert);
      }
    }
    return domainsToAlarm;
  }
}

function getStandardDeviation(array: number[]) {
  const n = array.length;
  if (n === 0) {
    return 0;
  }
  const mean = array.reduce((a, b) => a + b, 0) / n;
  return Math.sqrt(
    array.map(x => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / n
  );
}

function round1000(x: number) {
  return Math.round(x * 1000) / 1000;
}
