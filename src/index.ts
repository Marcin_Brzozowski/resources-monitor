#!/usr/bin/env node

import {Command} from 'commander';
import {makeAlertCommand} from './cmd/alert/alert';
import {makeScanCommand} from './cmd/scan/scan';
import {makeDomainCommand} from './cmd/domain/domain';

const program = new Command();

program.name('dm').description('Monitor and alarm usage of domain resources');

program.addCommand(makeAlertCommand());
program.addCommand(makeScanCommand());
program.addCommand(makeDomainCommand());

program.parse();
