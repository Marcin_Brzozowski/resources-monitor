# Documentation

## Installation

```
git clone https://gitlab.com/Marcin_Brzozowski/resources-monitor.git
cd resources-monitor
npm i
npm run compile
npm i -g .
```

## Configuration

You can see config options [here](docs/config.md)  
Ftp access options must be specified inside some separate text file with the following syntax:
```js
host: some-ftp-host
user: ftpuser
password: ftp-pass
```

## Base usage
If you want to use this cli as regular domain stats watcher you could use the following command along with **cron**: 
```
dm scan -p ftp_options.txt --latest
```

## Command Overview

| COMMAND                             | DESCRIPTION                                      |
|:------------------------------------|:-------------------------------------------------|
| [domain](docs/cmd/domain/domain.md) | Manage domain parameters                         |
| [scan](docs/cmd/scan/scan.md)       | Scan resources usage of domains on specific date |
| [alert](docs/cmd/alert/alert.md)    | Manage alerts                                    |

